# myMetro

## Intitulé du projet

Réaliser un **site internet responsive** dont l’objectif est de pouvoir consulter les **horaires de toutes les lignes des transports en commun de l’agglomération Grenobloise**.

## Contexte du projet

Utilisation de [l'API de la métro](https://www.mobilites-m.fr/pages/opendata/OpenDataApi.html)

## Contient

    * Une liste de toutes les lignes existantes avec leurs codes couleurs et leurs informations respectives (numéro de ligne /  destination / code couleur etc)
    * Le détail de chaque ligne (horaires / arrêts / etc...)
    * Une carte interactive présentant les informations suivantes :
        * Le tracé de la ligne
        * Les différents arrêts de la ligne
    * Système d'enregistrement de favoris
    * Système d'annonces (Prévention coronavirus / infos trafic temps réél)

## Workflow

    * Mobile-first
    * Trello

## Technologies

    * SASS (only css custom !)
    * Leaflet (geoJSON)
    * jQuery
    * npm
    * HTML5 / CSS3

## Getting started

    * Dans la console : 'npm install'.
    * Sur navigateur : lancer en premier le fichier index.html pour accéder à la feature de "première visite"

## WARNING

    * Le champ de recherche de la page d'accueil n'est pas encore fonctionnel.


**Auteur :** Jean MIONNET
**Date :** Vendredi 4 septembre 2020 
**Logo** désigné par Jean MIONNET

Le [prototype du projet](https://marvelapp.com/prototype/b620833) a été réalisé sur MarvelApp